var requestUrl = 'https://script.google.com/macros/s/AKfycbyOHUH2_nGWWACM0QID6v1pcpM9bOh1C6jqhDtufTEbg-8MRDYFxbf2xV1sDKVdVo7ibg/exec';
var form = document.getElementsByTagName('form')[0];
var submitButton = document.getElementsByClassName('button')[0];
var loader = document.getElementsByClassName('loader')[0];

form.addEventListener('submit', function (e) {
  e.preventDefault();
  var formData = new FormData(e.target);
  var body = {
    name: formData.get('name'),
    accept: formData.get('accept'),
    whiteVine: formData.get('whiteVine') === 'on',
    redVine: formData.get('redVine') === 'on',
    beer: formData.get('beer') === 'on',
    champagne: formData.get('champagne') === 'on',
    notDrink: formData.get('notDrink') === 'on'
  }

  var errorText = document.getElementsByClassName('field__error')[0];
  var field = document.getElementsByClassName('field__name')[0];
  if (!body.name) {
    errorText.classList.remove('none');
    field.style.borderColor = 'tomato';
    return;
  } else {
    errorText.classList.add('none');
    field.style.borderColor = 'black';
  }

  submitButton.disabled = true;
  loader.classList.add('show-loader');

  fetch(requestUrl, {
    redirect: "follow",
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "text/plain;charset=utf-8",
    }
  }).then(function (res) {
    return res.json();
  }).then(function (res) {
    if (res.status === 'success') {
      openModal('Спасибо!', 'Вы добавлены в список');
    } else {
      openModal('Ошибка!', 'Пожалуйста, попробуйте позже');
    }
  }).catch(function (e) {
    openModal('Ошибка!', 'Пожалуйста, попробуйте позже');
  }).finally(function () {
    submitButton.disabled = false;
    loader.classList.remove('show-loader');
  })
})

function openModal(title, text) {
  var modal = document.getElementsByClassName('modal')[0];

  if (modal) {
    document.getElementsByClassName('modal__title')[0].innerHTML = title;
    document.getElementsByClassName('modal__text')[0].innerHTML = text;

    modal.classList.add('flex');
    modal.classList.remove('none');

    document.getElementById('modal-overlay').addEventListener('click', closeModal);
    document.getElementById('modal-button').addEventListener('click', closeModal);
  }
}

function closeModal() {
  var modal = document.getElementsByClassName('modal')[0];

  if (modal) {
    modal.classList.add('none');
    modal.classList.remove('flex');
  }
}