var bgBubblesBox = document.getElementsByClassName('bubbles')[0];
var bubbles = bgBubblesBox.getElementsByClassName('bubble');

animation()
window.addEventListener('scroll', animation)

function animation(){
  Array.from(bubbles).forEach(function (bubble) {
    var bubbleParams = bubble.getBoundingClientRect();
    if (bubbleParams.y < window.screen.height / 10 * 8) {
      bubble.classList.remove('clear-size');
    }
  })
}