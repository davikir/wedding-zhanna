var days = document.getElementById('days');
var daysText = document.getElementById('daysText');
var hours = document.getElementById('hours');
var hoursText = document.getElementById('hoursText');
var minutes = document.getElementById('minutes');
var minutesText = document.getElementById('minutesText');
var seconds = document.getElementById('seconds');
var secondsText = document.getElementById('secondsText');

var targetDate = new Date(2024, 7, 10, 17).getTime();

var time = {
  days: 0,
  hours: 0,
  minutes: 0,
  seconds: 0
}

var intervalId = setInterval(setTime, 1000);

function setTime() {
  var curDate = new Date().getTime();
  var def = targetDate - curDate;

  if (def > 0) {
    var date = new Date(def);
    var temp = def
    time.days = Math.floor(temp / 86400000);
    temp -= time.days * 86400000;
    time.hours = Math.floor(temp / 3600000);
    temp -= time.hours * 3600000;
    time.minutes = Math.floor(temp / 60000);
    temp -= time.minutes * 60000;
    time.seconds = Math.floor(temp / 1000);

    days.innerHTML =time.days.toString();
    hours.innerHTML = time.hours.toString();
    minutes.innerHTML = time.minutes.toString();
    seconds.innerHTML = time.seconds.toString();
  } else {
    setStartText();
    clearInterval(intervalId);
  }
}

function setStartText() {
  var timeBox = document.getElementById('timeBox');
  timeBox.innerHTML = 'Началось!';
}