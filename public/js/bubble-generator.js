var BUBBLE_COLOR = 'rgba(255, 255, 255, 0.5)';
var BUBBLE_MIN_SIZE = 10;
var BUBBLE_MAX_SIZE = 20;
var BUBBLE_LIVE_TIME = 15000;
var BUBBLE_SPAWN_TIMEOUT = 2000;

function spawnBubble() {
  var bubbleInner = document.createElement('div');
  bubbleInner.classList.add('bubble__inner');
  var bubble = document.createElement('div');
  bubble.appendChild(bubbleInner);
  bubble.classList.add('bubble');

  var params = window.screen.width < 500 ? {factor: 1, unit: 'vw'} : {factor: 5, unit: 'px'};

  bubble.style.position = 'absolute';
  bubble.style.zIndex = '99';
  bubble.style.bottom = '0vh';
  bubble.style.left = getRandom(-5 * params.factor, 95 * params.factor) + params.unit;
  bubble.style.width = getRandom(BUBBLE_MIN_SIZE * params.factor, BUBBLE_MAX_SIZE * params.factor) + params.unit;
  bubble.style.backgroundColor = BUBBLE_COLOR;

  generateAnimation(bubble);

  document.getElementsByClassName('bubble-effect')[0].appendChild(bubble);

  setTimeout(() => {
    bubble.remove();
  }, BUBBLE_LIVE_TIME);
}

setInterval(spawnBubble, BUBBLE_SPAWN_TIMEOUT);

function generateAnimation(element) {
  var duration = getRandom(5000, BUBBLE_LIVE_TIME);
  element.style.animationName = 'bubble';
  element.style.animationDuration = duration + 'ms';
  element.style.animationTimingFunction = 'linear';
  element.style.animationIterationCount = '1';
  element.style.animationFillMode = 'forwards'
}

function getRandom(min, max) {
  return Math.round(Math.random() * (max - min)) + min;
}